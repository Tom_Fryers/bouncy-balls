import random


def gen_row(rownum, size):
    row = []
    for _ in range(size):
        # Block
        if random.random() < 0.4:
            # Boss block
            if random.random() < 0.005:
                row.append(random.randint(rownum * 2, rownum * 4))
            else:
                row.append(random.randint(rownum, round(rownum * 1.2 + 1)))
        # Power-up
        elif random.random() < 0.2:
            if random.random() < 0.03:
                row.append("shield")
            else:
                row.append(random.choice(["extraball", "ballspawn"]))
        # No block
        else:
            row.append(None)
    return row
