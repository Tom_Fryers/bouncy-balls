from dataclasses import dataclass

import pygame

from bouncy_balls import constants

pygame.init()
BLOCKSIZE = constants.BLOCKSIZE


@dataclass
class Ball:
    radius: float
    pos: pygame.math.Vector2
    vel: pygame.math.Vector2

    def move(self, timestep):
        self.pos += self.vel * timestep

    def draw(self, S, scroll):
        dp = (round(self.pos.x * BLOCKSIZE), round((self.pos.y + scroll) * BLOCKSIZE))
        pygame.draw.circle(S, (255, 255, 255), dp, round(self.radius * BLOCKSIZE))
