from dataclasses import dataclass, field

import pygame

from bouncy_balls import constants

pygame.init()
BLOCKSIZE = constants.BLOCKSIZE

COLS = {"extraball": (0, 100, 150), "ballspawn": (150, 100, 0), "shield": (100, 0, 150)}


@dataclass
class PowerUp:
    pos: pygame.math.Vector2
    effect: str
    radius: float = 0.3
    used: bool = False

    def collide(self, ball):
        if (self.pos - ball.pos).magnitude() < (self.radius + ball.radius):
            self.used = True
            return True
        return False

    def draw(self, S, scroll):
        dp = (round(self.pos.x * BLOCKSIZE), round((self.pos.y + scroll) * BLOCKSIZE))
        pygame.draw.circle(S, COLS[self.effect], dp, round(self.radius * BLOCKSIZE))
