import math
import random

import pygame
from pygame.locals import K_LSHIFT, K_RSHIFT, K_SPACE, KEYDOWN, QUIT, K_a, K_d

from bouncy_balls import ball, block, blockgen, constants, powerup

BLOCKSIZE = constants.BLOCKSIZE
FPS = constants.FPS
ITERS_PER_FRAME = 6
UPS = ITERS_PER_FRAME * FPS
SPEEDUP = 6
Vector = pygame.math.Vector2

BWIDTH = 10
BHEIGHT = 12

WIDTH = BLOCKSIZE * BWIDTH
HEIGHT = BLOCKSIZE * BHEIGHT
HEIGHT = BLOCKSIZE * BHEIGHT

BS = 6
AIMLEN = BLOCKSIZE * 3

EMPTYROWS = 6


def get_row_blocks(y):
    blocks = []
    powerups = []
    for x, item in enumerate(blockgen.gen_row(BHEIGHT - y - EMPTYROWS, BWIDTH)):
        if item is None:
            continue
        pos = Vector(x + 0.5, y + 0.5)
        if isinstance(item, str):
            powerups.append(powerup.PowerUp(pos, item))
        else:
            blocks.append(block.Block(pos, item))
    return blocks, powerups


def main():
    pygame.init()
    S = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Bouncy Balls")
    pygame.display.set_icon(pygame.image.load("icon.png"))
    play_game(S)


def play_game(S):
    clock = pygame.time.Clock()
    blocks = []
    balls = []
    powerups = []

    scroll = 0

    for y in range(BHEIGHT - EMPTYROWS):
        g = get_row_blocks(y)
        blocks += g[0]
        powerups += g[1]

    aim = 0
    ext = False
    totballs = 1
    fireclock = 0.1
    ballsleft = 0
    framenum = 0
    shield = 0
    current_shield = 0
    fire = False
    jd = False
    while not ext:
        if balls:
            newballs = []
            for b in balls:
                b.move(1 / UPS)
                for bl in blocks:
                    bl.collide(b)
                for p in powerups:
                    if p.collide(b):
                        if p.effect == "extraball":
                            totballs += 1
                        elif p.effect == "ballspawn":
                            n = random.randint(5, 6 + scroll // 5)
                            newballs += [
                                ball.Ball(
                                    0.05,
                                    p.pos + Vector(0, 0),
                                    Vector(
                                        math.cos(i * math.tau / n + 0.1),
                                        math.sin(i * math.tau / n + 0.1),
                                    )
                                    * BS,
                                )
                                for i in range(n)
                            ]
                        elif p.effect == "shield":
                            shield += 1
                        else:
                            raise ValueError(f"Invalid ball type: {p.effect}")
                if b.pos.x - b.radius < 0:
                    b.vel.reflect_ip(Vector(1, 0))
                    b.pos.x += (b.radius - b.pos.x) * 2

                elif b.pos.x + b.radius > BWIDTH:
                    b.vel.reflect_ip(Vector(1, 0))
                    b.pos.x += (BWIDTH - b.radius - b.pos.x) * 2

                if b.pos.y - b.radius < -scroll:
                    b.vel.reflect_ip(Vector(0, 1))
                    b.pos.y += (-scroll + b.radius - b.pos.y) * 2

                elif (
                    current_shield
                    and (
                        b.pos.y + scroll + b.radius
                        > BHEIGHT - 2 * current_shield // BLOCKSIZE
                    )
                    and b.vel.y > 0
                ):
                    b.vel.reflect_ip(Vector(0, 1))
                    b.pos.y += (
                        BHEIGHT
                        - 2 * current_shield // BLOCKSIZE
                        - b.radius
                        - b.pos.y
                        - scroll
                    ) * 2
                    current_shield -= 1

            blocks = [bl for bl in blocks if bl.hp > 0]
            balls = [
                b for b in balls + newballs if b.pos.y + scroll - b.radius < BHEIGHT
            ]
            if not balls:
                jd = True
            powerups = [p for p in powerups if not p.used]

        # New row
        if jd:
            jd = False
            scroll += 1
            g = get_row_blocks(-scroll)
            blocks += g[0]
            powerups += g[1]

        # Start firing balls
        elif fire:
            fire = False
            ballsleft = totballs
            fireclock = 0
            current_shield = shield

        # Fire a ball
        if ballsleft > 0:
            fireclock -= 1 / UPS
            if fireclock <= 0:
                balls.append(
                    ball.Ball(
                        0.1,
                        Vector(BWIDTH / 2, BHEIGHT - scroll),
                        Vector(math.sin(aim) * BS, -math.cos(aim) * BS),
                    )
                )
                fireclock = 0.1
                ballsleft -= 1

        # Check for loss
        for b in blocks:
            if b.pos.y + scroll >= BHEIGHT - 1:
                return

        framenum += 1
        if not framenum % (ITERS_PER_FRAME * SPEEDUP) or (
            not pygame.key.get_pressed()[K_LSHIFT] and not framenum % (ITERS_PER_FRAME)
        ):
            fire = False
            jd = False
            for event in pygame.event.get():
                if event.type == QUIT:
                    ext = True
                    break
                if event.type == KEYDOWN:
                    if event.key == K_SPACE and not balls:
                        fire = True
                    elif event.key == K_RSHIFT and balls:
                        balls = []
                        jd = True
                        fire = False
            if ballsleft == 0:
                if pygame.key.get_pressed()[K_a]:
                    aim -= 0.8 / FPS
                if pygame.key.get_pressed()[K_d]:
                    aim += 0.8 / FPS
            aim = max(-1.3, min(1.3, aim))
            # Draw everything
            S.fill((0, 0, 0))
            for bl in blocks:
                bl.draw(S, scroll)
            for b in balls:
                b.draw(S, scroll)
            for p in powerups:
                p.draw(S, scroll)
            for i in range(current_shield):
                pygame.draw.line(
                    S,
                    (255, 255, 255),
                    (0, HEIGHT - i * 2 - 1),
                    (WIDTH, HEIGHT - i * 2 - 1),
                    1,
                )
            if not balls:
                pygame.draw.line(
                    S,
                    (100, 200, 100),
                    (round(WIDTH / 2), HEIGHT),
                    (
                        round(WIDTH / 2 + math.sin(aim) * AIMLEN),
                        round(HEIGHT - math.cos(aim) * AIMLEN),
                    ),
                    2,
                )

            pygame.display.update()
            clock.tick(FPS)
