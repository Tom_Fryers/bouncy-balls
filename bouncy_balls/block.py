import colorsys

import pygame

from bouncy_balls import constants

pygame.init()
Vector = pygame.math.Vector2
BLOCKSIZE = constants.BLOCKSIZE

FONTFBH = pygame.font.Font(None, round(BLOCKSIZE * 0.8))


def block_colour(hp):
    hue = hp * 5
    saturation = min(50 + hp / 72 * 20, 100)
    value = 50
    return [
        255 * x for x in colorsys.hsv_to_rgb(hue / 360, saturation / 100, value / 100)
    ]


class Block:
    def __init__(self, pos, hp):
        self.pos = pos
        self.hp = hp
        self.update_image()
        self.left = self.pos.x - 0.5
        self.right = self.pos.x + 0.5
        self.top = self.pos.y - 0.5
        self.bottom = self.pos.y + 0.5

    def draw(self, S, scroll):
        S.blit(self.image, (self.left * BLOCKSIZE, (self.top + scroll) * BLOCKSIZE))

    def damage(self):
        self.hp -= 1
        self.update_image()

    def update_image(self):
        self.image = pygame.Surface((BLOCKSIZE, BLOCKSIZE))
        self.image.fill(block_colour(self.hp))
        hpim = FONTFBH.render(str(self.hp), 1, (0, 0, 0))
        self.image.blit(
            hpim,
            ((BLOCKSIZE - hpim.get_width()) / 2, (BLOCKSIZE - hpim.get_height()) / 2),
        )

    def collide(self, ball):
        # Check for possible collision
        if (
            self.left - ball.radius < ball.pos.x < self.right + ball.radius
            and self.top - ball.radius < ball.pos.y < self.bottom + ball.radius
        ):
            # Do edge collisions

            # Horizontal edges:
            if self.left < ball.pos.x < self.right:
                ball.vel.reflect_ip(Vector(0, 1))
                self.damage()
                return

            # Vertical edges:
            if self.top < ball.pos.y < self.bottom:
                ball.vel.reflect_ip(Vector(1, 0))
                self.damage()
                return

            # Do corner collisions
            for corner in (
                self.pos + offset
                for offset in (
                    Vector(-0.5, -0.5),
                    Vector(0.5, -0.5),
                    Vector(-0.5, 0.5),
                    Vector(0.5, 0.5),
                )
            ):
                distance = (corner - ball.pos).magnitude()
                if distance < ball.radius:
                    ball.vel.reflect_ip(corner - ball.pos)
                    # Move ball back from corner
                    ball.pos += (
                        (ball.pos - corner).normalize() * (ball.radius - distance) * 2
                    )
                    self.damage()
                    return

    def __repr__(self):
        return f"Block({self.pos}, {self.hp})"
