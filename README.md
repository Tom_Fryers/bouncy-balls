# Bouncy Balls

This game requires Python and Pygame.

Run `run_game.py` to launch the game.

Use A and D to aim, and press space to fire a stream of balls. Left shift
speeds up time, and right shift destroys all currently bouncing balls, in case
they are taking too long to disappear.
